package info.guardianproject.proofmode.sample

//add ProofMode import
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Base64
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import info.guardianproject.proofmode.sample.databinding.ActivityMainBinding
import org.bouncycastle.openpgp.PGPException
import org.witness.proofmode.ProofMode
import org.witness.proofmode.crypto.pgp.PgpUtils
import org.witness.proofmode.notarization.NotarizationListener
import org.witness.proofmode.notarization.NotarizationProvider
import org.witness.proofmode.service.MediaWatcher
import org.witness.proofmode.storage.DefaultStorageProvider
import org.witness.proofmode.storage.StorageListener
import org.witness.proofmode.storage.StorageProvider
import timber.log.Timber
import timber.log.Timber.Forest.plant
import java.io.*
import java.util.ArrayList
import java.util.concurrent.Executors
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    private val TAG = "ProofSample"
    private lateinit var lastProofHash : String

    //use the default file system based storage provider
   private val mStorageProvider = DefaultStorageProvider(this)

    /**
     * // or implement a custom storage provider
    private val mStorageProvider = object : StorageProvider {
        override fun saveStream(hash: String?, identifier: String?, p2: InputStream?, p3: StorageListener?) {
            TODO("Not yet implemented")
        }

        override fun saveBytes(hash: String?, identifier: String?, p2: ByteArray?, p3: StorageListener?) {
            TODO("Not yet implemented")
        }

        override fun saveText(hash: String?, identifier: String?, p2: String?, p3: StorageListener?) {
            TODO("Not yet implemented")
        }

        override fun getInputStream(hash: String?, identifier: String?): InputStream {
            TODO("Not yet implemented")
        }

        override fun getOutputStream(hash: String?, identifier: String?): OutputStream {
            TODO("Not yet implemented")
        }

        override fun proofExists(hash: String?): Boolean {
            TODO("Not yet implemented")
        }

        override fun proofIdentifierExists(hash: String?, identifier: String?): Boolean {
            TODO("Not yet implemented")
        }

        override fun getProofSet(hash: String?): ArrayList<Uri> {
            TODO("Not yet implemented")
        }

        override fun getProofItem(hash: Uri?): InputStream {
            TODO("Not yet implemented")
        }

    }*/

    private val password = "default"

    // Registers a photo picker activity launcher in single-select mode.
    val pickMedia = registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
        // Callback is invoked after the user selects a media item or closes the
        // photo picker.
        if (uri != null) {
            Timber.tag(TAG).d("Selected URI: $uri")

            //call the addProof method defined in this activity
            lastProofHash = addProof(uri);

            findViewById<TextView>(R.id.textview_first).text = "Proof Generated: $lastProofHash"

            findViewById<Button>(R.id.btn_share).visibility = View.VISIBLE

        } else {
            Timber.tag(TAG).d("No media selected")
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (BuildConfig.DEBUG) {
            plant(Timber.DebugTree())
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.fab.setOnClickListener { view ->

// Launch the photo picker and allow the user to choose images and videos.
            pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageAndVideo))
        }

        findViewById<Button>(R.id.btn_share).setOnClickListener {

            //create a zip file of Proof using the hash
           // var proofDir = ProofMode.getProofDir(this, lastProofHash)

           val proofDir = mStorageProvider.getHashStorageDir(lastProofHash)
            if (proofDir != null && proofDir.exists()) {
                val fileZip = makeProofZip(proofDir.absoluteFile)
                Timber.tag(TAG).d("zip path: $fileZip");
                shareFile(fileZip)
            }
        }

        //make sure to set our storage provider
        MediaWatcher.getInstance(this).setStorageProvider(mStorageProvider)

        //enable the proof points you want and can support
        var proofDeviceIds = false; //requires additional permission
        var proofLocation = false; //requires additional permission
        var proofNetwork = false; //requires additional permission
        var proofNotary = true; //contacts third-party services - opentimestamps, google safetynet

        ProofMode.setProofPoints(this, proofDeviceIds, proofLocation, proofNetwork, proofNotary);

        ProofMode.addNotarizationProvider(this, object : NotarizationProvider {
            override fun notarize(
                mediaHash: String?,
                inputStream: String?,
                inpustream2: InputStream?,
                listener: NotarizationListener?
            ) {
                var notaryFakeResponse = "looks good to me"

                listener?.notarizationSuccessful(mediaHash,
                    Base64.encode(notaryFakeResponse.toByteArray(Charsets.UTF_8), Base64.DEFAULT).toString()
                );
            }

            override fun getProof(mediaHash: String?): String {
                TODO("Not yet implemented")
                //not required
            }

            override fun getNotarizationFileExtension(): String {
                return "foo";
            }

        })

        checkAndGeneratePublicKey()
    }

    fun checkAndGeneratePublicKey() {
        Executors.newSingleThreadExecutor().execute {

            //Background work here
            var pubKey: String? = null
            try {
                pubKey = PgpUtils.getInstance(applicationContext, password).publicKeyFingerprint
                showToastMessage("public key generated: $pubKey")
            } catch (e: PGPException) {
                Timber.e(e, "error getting public key")
                showToastMessage("error generating public key: $e.message")
            } catch (e: IOException) {
                Timber.e(e, "error getting public key")
                showToastMessage("error generating public key: $e.message")
            }
        }
    }

    private fun showToastMessage(message: String) {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            //UI Thread work here
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    // Generate proof and use returned to has to find path where proof is stored
    fun addProof(uriMedia: Uri): String {

        var proofHash = ProofMode.generateProof(this,uriMedia)
        Timber.tag(TAG).d("proof generated: $proofHash")

        var proofDir = mStorageProvider.getHashStorageDir(proofHash)

        Timber.tag(TAG).d("proof local dir: $proofDir")

        if (proofHash == null)
        {
            return ""
        }
        else
            return proofHash
    }

    // make zip file of local proof files
    fun makeProofZip(proofDirPath: File): File {
        val outputZipFile = File(filesDir, proofDirPath.name + ".zip")
        ZipOutputStream(BufferedOutputStream(FileOutputStream(outputZipFile))).use { zos ->
            proofDirPath.walkTopDown().forEach { file ->
                val zipFileName = file.absolutePath.removePrefix(proofDirPath.absolutePath).removePrefix("/")
                val entry = ZipEntry( "$zipFileName${(if (file.isDirectory) "/" else "" )}")
                zos.putNextEntry(entry)
                if (file.isFile) {
                    file.inputStream().copyTo(zos)
                }
            }

            val keyEntry = ZipEntry("pubkey.asc");
            zos.putNextEntry(keyEntry);
            var publicKey = ProofMode.getPublicKeyString(this, password)
            zos.write(publicKey.toByteArray());

        }

        return outputZipFile
    }

    fun shareFile (file : File) {
        try {
            if(file.exists()) {
                val uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", file)
                val intent = Intent(Intent.ACTION_SEND)
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                intent.setType("*/*")
                intent.putExtra(Intent.EXTRA_STREAM, uri)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()

        }
    }
}